#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <cstdlib>
#include <tuple>
#include <vector>

#include "easylogging++.h"
#include "OilEffect.h"


// Take img path, radius, intensity levels, and tuple containing information about resizing the image
// resize_img(0) -> do we want to resize, resize_img(1,2) -> (w,h) we wnd to resize to
cv::Mat OilEffect(  const std::string& path, int radius, const int &intensityLevels,
                    const std::tuple<bool, int, int> &resize_image)
{

    // Load image
    cv::Mat input_image, img_oil;
    input_image = cv::imread(path);

    // Unpack tuple
    const auto [resize, resized_width, resized_height] = resize_image;

    // If we want to resize, get the scaling ratio and size our input radius with it
    // (This is so that the preview is somewhat similar to the final output)
    if (resize)
    {
        const float ratio = (float)resized_width / (float)input_image.cols;
        cv::resize( input_image, input_image, cv::Size(resized_width, resized_height) );

        radius *= ratio;
        if (radius <= 0)
        { 
            radius = 1;
            LOG(WARNING) << "Radius chosen was too small to compute a correct preview";
        }


    }

    //Copy the original to our canvas
    input_image.copyTo(img_oil);

    // Create mask with our circle shape
    std::vector<std::tuple<int, int>> mask;
    for (int i = -radius; i < radius; ++i){
        for (int j = -radius; j < radius; ++j){
            if ( i*i + j*j <= radius*radius ){
                mask.push_back(std::tuple<int, int> (i, j));
            }
        }
    }

    // Define what a Pixel is
    typedef cv::Point3_<uint8_t> Pixel;

    const int height = img_oil.rows, width = img_oil.cols;
    //Loop through all the pixels
    input_image.forEach<Pixel>([&](Pixel& pixel, const int position[]) -> void {

        //Initialize couters and averages to 0
        std::vector<int> intensityCount( intensityLevels, 0 );
        std::vector<int> averageR( intensityLevels, 0 ), averageG( intensityLevels, 0 ), averageB( intensityLevels, 0 );

        // unpack the positions variables for readability
        const int i = position[1], j = position[0];

        //Loop over the pixels in the radius
        for (std::vector<std::tuple<int,int>>::iterator it = mask.begin();
                it != mask.end(); it++){
            

            // Unpack the coordinate in the mask and add them to current position in image
            auto [x, y] = *it;
            x += i; y += j;

            // Test if we are in bounds
            if (x < 0 || y < 0 || x > width || y > height){
                continue;
            }

            // Compute intensity and add values in correct bins
            const int curIntensity = computeIntensity(x, y, intensityLevels, input_image);
            intensityCount[curIntensity]++;
            const cv::Vec3b color = input_image.at<cv::Vec3b>(y, x);
            averageR[curIntensity] += color[2];
            averageG[curIntensity] += color[1];
            averageB[curIntensity] += color[0];

            //Compute the argmax of intensity bins
            const int max_index = std::distance(intensityCount.begin(), std::max_element(intensityCount.begin(), intensityCount.end() ));

            //Compute final values and apply them to the oil painting
            int finalR = averageR[max_index], finalG = averageG[max_index], finalB = averageB[max_index];
            int curMax = intensityCount[max_index];
            finalR /= curMax; finalG /= curMax; finalB /= curMax;

            // Get memory of pixel we want to change and modify it
            cv::Vec3b *ptr = img_oil.ptr<cv::Vec3b>(j, i);
            (*ptr)[0] = finalB;
            (*ptr)[1] = finalG;
            (*ptr)[2] = finalR;
        }
    });

    return img_oil;
}

// Take img and coordinate and compute intensity of its pixel
int computeIntensity(const int &x, const int &y, const int &intensityLevels, const cv::Mat &image)
{
    // Access pixel and compute its intensity
    const cv::Vec3b pix = image.at<cv::Vec3b>(y, x);
    const double val = ( intensityLevels - 1)*( pix[0] + pix[1] + pix[2] ) / 765.0;   // 765 = 3 * 255
    return (int)val;
}