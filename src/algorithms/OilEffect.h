#pragma once

#include <opencv2/core.hpp>
#include <tuple>


int computeIntensity(const int &x, const int &y, const int &intensityLevels, const cv::Mat &image);
cv::Mat OilEffect(const std::string& path, int radius, const int &intensityLevels, const std::tuple<bool, int, int> &resize_image);