#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

#include <iostream>

#include "easylogging++.h"
#include "RandEllipses.h"




cv::Mat RandEllipses(const std::string& path, int radius, const int &ellipsesRatio,
                    const int &numberOfEllipses, const bool &useGrad,
                    std::tuple<bool, int, int> resize_image)
{
    cv::Mat input_img;

    input_img = cv::imread(path);

    // Unpack tuple
    const auto [resize, resized_width, resized_height] = resize_image;

    //If we have to resize it
    if (resize)
    {

        float ratio = (float)resized_width / (float)input_img.cols;
        cv::resize( input_img, input_img, cv::Size(resized_width, resized_height) );
        //If we resize the image we also have to resize the radius we work with
        radius *= ratio;
        if (radius < 1)
        { 
            radius = 1;
            LOG(WARNING) << "Radius chosen was too small to compute a correct preview";
        }


    }

    // Compute grad on a greyscaled blured image if user wants it
    cv::Mat gray, dx, dy;
    if (useGrad)
    {
        cv::cvtColor(input_img, gray, cv::COLOR_BGR2GRAY);
        cv::GaussianBlur(gray, gray, cv::Size(3,3),0);
        cv::Scharr(gray, dx, -1, 1, 0);
        cv::Scharr(gray, dy, -1, 0, 1);
        cv::imwrite("dx.jpg", dx);
        cv::imwrite("dy.jpg", dy);
    }




    cv::GaussianBlur(input_img, input_img, cv::Size(15, 15), 0);

    //Create white canvas
    cv::Mat canvas = cv::Mat::zeros( cv::Size(input_img.cols, input_img.rows), input_img.type());
    

    //Add our ellipses to the canvas
    int xmax = canvas.cols, ymax = canvas.rows;
    for (int i=0; i < numberOfEllipses; i++)
    {

        //Generate random (x,y) coordinate
        int x = (rand() % static_cast<int>(xmax + 1));
        int y = (rand() % static_cast<int>(ymax + 1));

        int theta = 0;
        if (useGrad) theta = computeAngle(x, y, dx, dy);
        else theta = (rand() % static_cast<int>(181));

        //Get the color at the actual pixel

        //Draw the ellipse on the given coordinate with the color on the BLURRED CANVAS
        cv::ellipse(canvas, cv::Point(x,y), cv::Size(radius, radius * ellipsesRatio), theta, 0, 360, 
            cv::Scalar(input_img.at<cv::Vec3b>(y,x)[0], input_img.at<cv::Vec3b>(y,x)[1], input_img.at<cv::Vec3b>(y,x)[2]),
             -1, cv::LINE_AA);

    }

    return canvas;

}

int computeAngle(const int &x, const int &y, const cv::Mat &xgrad, const cv::Mat &ygrad)
{
    const uchar dx = xgrad.at<u_char>(y, x);
    const uchar dy = ygrad.at<u_char>(y, x);
    // Normalised emplitudes
    const float fx = (float)dx / 255.0f, fy = (float)dy / 255.0f;

    if (fx == 0 && fy == 0)
        return (rand() % static_cast<int>(181));

    if (fx == 0)
        return (int)(fy * 90.0f);

    return (int)(180.0 * atan( fy / fx ) / 3.141592);
}