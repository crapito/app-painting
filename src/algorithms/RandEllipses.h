#pragma once

#include <opencv2/imgproc.hpp>
#include <tuple>

cv::Mat RandEllipses(const std::string& path, int radius, const int &ellipsesRatio, const int &numberOfEllipses, const bool &useGrad, std::tuple<bool, int, int> resize_image);
int computeAngle(const int &x, const int &y, const cv::Mat &xgrad, const cv::Mat &ygrad);