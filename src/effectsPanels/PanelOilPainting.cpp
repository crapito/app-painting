#include "PanelOilPainting.h"
#include "../ToolPanel.h"

#include "../algorithms/OilEffect.h"
#include "easylogging++.h"
#include <chrono>

/*
We can allocate IDs ranging from 10300 to 10399 included
*/

wxBEGIN_EVENT_TABLE(PanelOilPainting, wxPanel)
    EVT_BUTTON(10302, PanelOilPainting::ComputePreview)
    EVT_BUTTON(10303, PanelOilPainting::ComputeFullRender)
wxEND_EVENT_TABLE()


PanelOilPainting::PanelOilPainting(wxChoicebook *parent)
    : wxPanel(parent, wxID_ANY, wxPoint(-1,-1), wxSize(-1,-1), wxBORDER_SUNKEN)
{
    m_parent = parent;


    //Create the horizontal sizers
    m_radius = new wxSpinCtrl(this, 10300, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 1000, 5);
    wxStaticText *m_radius_text = new wxStaticText(this, wxID_ANY, "Radius:");
    wxBoxSizer *sizerRadius = new wxBoxSizer(wxHORIZONTAL);
    sizerRadius->Add(m_radius_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerRadius->Add(m_radius,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);


    m_intensityBins = new wxSpinCtrl(this, 10301, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 20);
    wxStaticText *m_intensityBins_text = new wxStaticText(this, wxID_ANY, "IntensityBins:");
    wxBoxSizer *sizerIntensityBins = new wxBoxSizer(wxHORIZONTAL);
    sizerIntensityBins->Add(m_intensityBins_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerIntensityBins->Add(m_intensityBins,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_buttonPreview = new wxButton(this, 10302, "Preview");
    m_buttonRender = new wxButton(this, 10303, "Full Render and save");

    wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);
    boxSizer->Add(sizerRadius, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(sizerIntensityBins, 1, wxEXPAND | wxALL, 5);
    boxSizer->Add(m_buttonPreview, 1, wxSHAPED | wxALL | wxALIGN_RIGHT, 5);
    boxSizer->Add(m_buttonRender, 1, wxSHAPED | wxALL | wxALIGN_RIGHT, 5);

    this->SetSizer(boxSizer);

}


void PanelOilPainting::ComputePreview(wxCommandEvent &evt)
{
    
    // First check is something is loaded, otherwise abort
    ToolPanel *toolpanel = (ToolPanel *) m_parent->GetParent();
    const bool is_image_loaded = toolpanel->isImageLoaded();
    if ( !is_image_loaded )
    {
        //TODO print some error and tell user to load file first
        LOG(WARNING) << "Image not Loaded yet";
        return;
    }
    
    // Get all the variables from the panels
    const wxString path = toolpanel->GetImagePath();
    const auto [w, h] = toolpanel->GetLowResDim();
    const int   radius = m_radius->GetValue(),
                intensityLevels = m_intensityBins->GetValue();

    // TODO: Clean up timer
    auto start = std::chrono::steady_clock::now();

    // Run the computation
    cv::Mat image_preview_cv =
        OilEffect( path.ToStdString(), radius, intensityLevels, std::tuple<bool, int, int>(1, w, h));

    // Get elapsed time and log it
    auto end = std::chrono::steady_clock::now();
    LOG(INFO)   << "OilEffect preview computation with:\n"
                << "Radius: " << radius
                << ", Intensity: " << intensityLevels
                << "\nDone in: " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << " ms";

    // Convert to wxImage and set image to preview panel
    wxImage image_preview = toolpanel->Mat_to_wxImage(image_preview_cv);
    toolpanel->SetImagePreview(image_preview);

    evt.Skip();
}


void PanelOilPainting::ComputeFullRender(wxCommandEvent &evt)
{
    // First check is something is loaded, otherwise abort
    ToolPanel *toolpanel = (ToolPanel *) m_parent->GetParent();
    const bool is_image_loaded = toolpanel->isImageLoaded();
    if ( !is_image_loaded )
    {
        //TODO print some error and tell user to load file first
        LOG(WARNING) << "Image not Loaded yet";
        return;
    }

    // Get all information needed from panels
    const wxString path = toolpanel->GetImagePath();
    const int   radius = m_radius->GetValue(),
                intensityLevels = m_intensityBins->GetValue();

    // Start a chrono
    auto start = std::chrono::steady_clock::now();

    // Lauch the heavy computation
    cv::Mat rendered_image = OilEffect( path.ToStdString(), radius, intensityLevels, std::tuple<bool, int, int>(0, 0, 0));

    // Get elapsed time and log it
    auto end = std::chrono::steady_clock::now();
    LOG(INFO)   << "OilEffect full computation with:\n"
                << "Radius: " << radius
                << ", Intensity: " << intensityLevels
                << "\nDone in: " << std::chrono::duration_cast<std::chrono::seconds>(end-start).count() << " s";


    // Save resulting image
    toolpanel->SaveImage(rendered_image);
    // TODO: Also set it (a sized down version) into the preview panel


    /*
    TODO
    Ideally we want to send this load on another thread and show some loading animation somewhere
    Maybe make a general function in the ToolPanel to send heavy load, this function would handle some kind of
    qeueing system so that no heavy work is given if another is already in motion.

    We would have something like
    toolpanel->HANDLE_LOAD(    HEAVY_ASS_FUNCTION(ARGS)    )
    
    If we have something cooking we could have it ask if we want to stop the last task or just wait
    */


    evt.Skip();
}