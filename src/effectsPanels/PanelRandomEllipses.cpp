#include <chrono>
#include "PanelRandomEllipses.h"
#include "../ToolPanel.h"

#include "../algorithms/RandEllipses.h"
#include "easylogging++.h"

/*
We can allocate IDs ranging from 10400 to 10499 included
*/

wxBEGIN_EVENT_TABLE(PanelRandomEllipses, wxPanel)
    EVT_BUTTON(10402, PanelRandomEllipses::ComputePreview)
    EVT_BUTTON(10403, PanelRandomEllipses::ComputeFullRender)
wxEND_EVENT_TABLE()

PanelRandomEllipses::PanelRandomEllipses(wxChoicebook *parent)
    : wxPanel(parent, wxID_ANY, wxPoint(-1,-1), wxSize(-1,-1), wxBORDER_SUNKEN)
{
    m_parent = parent;

    //Sizer that will contain all the little sizers
    wxBoxSizer *boxSizer = new wxBoxSizer(wxVERTICAL);

    //Creates the Controls and put them in an horizontal sizer with corresponding text
    m_radius = new wxSpinCtrl(this, 10400, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 1000, 5);
    wxStaticText *m_radius_text = new wxStaticText(this, wxID_ANY, "Radius:");
    wxBoxSizer *sizerRadius = new wxBoxSizer(wxHORIZONTAL);
    sizerRadius->Add(m_radius_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerRadius->Add(m_radius,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);


    m_ellipsesRatio = new wxSpinCtrl(this, 10401, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 100, 5);
    wxStaticText *m_ellipsesRatio_text = new wxStaticText(this, wxID_ANY, "Ellipses Ratio:");
    wxBoxSizer *sizerEllipsesRatio = new wxBoxSizer(wxHORIZONTAL);
    sizerEllipsesRatio->Add(m_ellipsesRatio_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerEllipsesRatio->Add(m_ellipsesRatio,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);


    m_numberOfEllipses = new wxSpinCtrl(this, 10404, "", wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 1, 26, 15);
    wxStaticText *m_numberOfEllipses_text = new wxStaticText(this, wxID_ANY, "Number of ellipses (power of 2):");
    wxBoxSizer *sizerNumberOfEllipses = new wxBoxSizer(wxHORIZONTAL);
    sizerNumberOfEllipses->Add(m_numberOfEllipses_text,1, wxALL | wxALIGN_LEFT, 0);
    sizerNumberOfEllipses->Add(m_numberOfEllipses,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);

    m_usegradiant = new wxCheckBox(this, wxID_ANY, "", wxDefaultPosition, wxDefaultSize);
    wxStaticText *m_gradiant_text = new wxStaticText(this, wxID_ANY, "Use gradiant: ");
    wxBoxSizer *sizerGradiantUse = new wxBoxSizer(wxHORIZONTAL);
    sizerGradiantUse->Add(m_gradiant_text, 1, wxALL | wxALIGN_LEFT, 0);
    sizerGradiantUse->Add(m_usegradiant,3, wxALL | wxSHAPED | wxALIGN_RIGHT, 0);


    //Creates the Render buttons
    m_buttonPreview = new wxButton(this, 10402, "Preview");
    m_buttonRender = new wxButton(this, 10403, "Full Render");




    //Add everything into the Sizer
    boxSizer->Add(sizerRadius, 2, wxEXPAND | wxALL , 5);
    boxSizer->Add(sizerEllipsesRatio, 2, wxEXPAND | wxALL , 5);
    boxSizer->Add(sizerNumberOfEllipses, 2, wxEXPAND | wxALL , 5);
    boxSizer->Add(sizerGradiantUse, 2, wxEXPAND | wxALL , 5);
    boxSizer->Add(m_buttonPreview, 1, wxSHAPED | wxALL | wxALIGN_RIGHT, 5);
    boxSizer->Add(m_buttonRender, 1, wxSHAPED | wxALL | wxALIGN_RIGHT, 5);

    this->SetSizer(boxSizer);

}


void PanelRandomEllipses::ComputePreview(wxCommandEvent &evt)
{
    // Check if something is loaded, abort otherwise
    ToolPanel *toolpanel = (ToolPanel *) m_parent->GetParent();
    const bool is_image_loaded = toolpanel->isImageLoaded();
    if ( !is_image_loaded )
    {
        //TODO print some error and tell user to load file first
        LOG(WARNING) << "Image not Loaded yet";

        return;
    }

    // Get image path and the dimension of lower resolution preview img
    const wxString path = toolpanel->GetImagePath();
    const auto [w, h] = toolpanel->GetLowResDim();


    // Get all values from panel
    const int   radius = m_radius->GetValue(),
                numberOfEllipses = m_numberOfEllipses->GetValue(),
                ellipsesRatio = m_ellipsesRatio->GetValue();
    const bool  useGrad = m_usegradiant->GetValue();

    // Time execution
    auto start = std::chrono::steady_clock::now();

    cv::Mat image_preview_cv = RandEllipses( path.ToStdString(), radius, ellipsesRatio,
            std::pow(2,numberOfEllipses), useGrad,
            std::tuple<bool, int, int>(1, w, h));

    // Log time it took
    auto end = std::chrono::steady_clock::now();
    LOG(INFO)   << "Random ellipses preview: " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << " ms"
                << ", using:"   << " Radius: " << radius 
                                << " Ratio: " << ellipsesRatio
                                << " Quantity: " << std::pow(2, numberOfEllipses)
                                << " Gradient " << useGrad;

    wxImage image_preview = toolpanel->Mat_to_wxImage(image_preview_cv);

    toolpanel->SetImagePreview(image_preview);

    evt.Skip();
}


void PanelRandomEllipses::ComputeFullRender(wxCommandEvent &evt)
{
    // Check if something is loaded, abort otherwise
    ToolPanel *toolpanel = (ToolPanel *) m_parent->GetParent();
    bool is_image_loaded = toolpanel->isImageLoaded();
    if ( !is_image_loaded )
    {
        //TODO print some error and tell user to load file first
        LOG(WARNING) << "Image not Loaded yet";
        return;
    }

    // Fetch the required variables from panel
    wxString path = toolpanel->GetImagePath();
    const int   radius = m_radius->GetValue(),
                numberOfEllipses = m_numberOfEllipses->GetValue();
    const float ellipsesRatio = m_ellipsesRatio->GetValue();
    const bool  useGrad = m_usegradiant->GetValue();

    // Use to log time
    auto start = std::chrono::steady_clock::now();

    cv::Mat full_image = RandEllipses( path.ToStdString(), radius, ellipsesRatio,
                std::pow(2,numberOfEllipses), useGrad, std::tuple<bool, int, int>(false, 0, 0));

    // Log time it took
    auto end = std::chrono::steady_clock::now();
    LOG(INFO)   << "Random ellipses preview: " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << " ms"
                << ", using:"   << " Radius: " << radius 
                                << " Ratio: " << ellipsesRatio
                                << " Quantity: " << std::pow(2, numberOfEllipses)
                                << " Gradient " << useGrad;

    // Save img and set it to the preview panel
    toolpanel->SaveImage(full_image);
    wxImage preview_img = toolpanel->Mat_to_wxImage(full_image);
    toolpanel->SetImagePreview( preview_img );



    evt.Skip();
}