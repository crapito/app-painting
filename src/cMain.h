#pragma once

#include <wx/wx.h>
#include <wx/splitter.h>
#include <tuple>

#include "ImagePanel.h"
#include "ToolPanel.h"


class cMain : public wxFrame
{
public:
    cMain();
    ~cMain();

private:

    wxMenuBar *m_MenuBar = nullptr;
    ImagePanel *m_imagepanel = nullptr;
    ToolPanel *m_toolpanel = nullptr;

    void LoadImage(wxCommandEvent &evt);
    void SendZoomRequest(wxCommandEvent &evt);

public:
    //Functions used to comunicate between the ToolPanel and the ImagePanel
    bool isImageLoaded() const;
    wxString GetImagePath() const;
    std::tuple<int, int> GetLowResDim() const;
    void SetImagePreview(wxImage &preview);

    //Receive a KeyCode on event KEY_DOWN and handles it
    bool OnKeyDown(int KeyCode);


    wxDECLARE_EVENT_TABLE();
};