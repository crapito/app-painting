#include <opencv2/imgproc.hpp>
#include "ToolPanel.h"
#include "cMain.h"

#include "easylogging++.h"

/*
We can allocate IDs ranging from 10200 to 10299 included
*/

ToolPanel::ToolPanel(wxSplitterWindow *parent)
    : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(-1, -1), wxBORDER_SUNKEN)
{
    m_parent = parent;

    // Create choice book and populate it with our pages
    m_choicebook = new wxChoicebook(this, wxID_ANY);

    PanelOilPainting *page1 = new PanelOilPainting(m_choicebook);
    m_choicebook->AddPage(page1, "Oil Painting");

    PanelRandomEllipses *page2 = new PanelRandomEllipses(m_choicebook);
    m_choicebook->AddPage(page2, "Random Ellipses");

    PanelPointillism *page3 = new PanelPointillism(m_choicebook);  
    m_choicebook->AddPage(page3, "Pointillism");

    // Set up the sizer for the panel
    wxBoxSizer* panelSizer = new wxBoxSizer(wxHORIZONTAL);
    panelSizer->Add(m_choicebook, 1, wxEXPAND);
    this->SetSizer(panelSizer);
}


// Convert cv::Mat into wxImage
// To do so, we have to manually allocate memory, put data into it, then create
// wxImage and tell it where to take its data
wxImage ToolPanel::Mat_to_wxImage(cv::Mat &img) const
{
    // Compute size of memory we have to allocate to the data
    int w = img.cols;
    int h = img.rows;
    int size = w * h * 3 * sizeof(uchar);

    // allocate memory
    uchar* wxData = (uchar*) malloc(size);

    // the matrix stores BGR image for conversion
    cv::Mat cvRGBImg;
    
    switch (img.channels())
    {
        case 3: // 3-channel case: swap R&B channels
        {
            cvRGBImg = cv::Mat(h, w, CV_8UC3, wxData);
            int mapping[] = {0,2,1,1,2,0}; // CV(BGR) to WX(RGB)
            cv::mixChannels(&img, 1, &cvRGBImg, 1, mapping, 3);
        } break;
        default:
        {
            LOG(ERROR) << "ToolPanel::Mat_to_wxImage:\ninput image (#channel=" << img.channels() << ") should be 3-channel";
            return wxImage(w,h);
        }
    }

    return wxImage(w, h, wxData);
}

//TODO: Request file name or file destination to cMain
void ToolPanel::SaveImage(cv::Mat &img)
{
    if (cv::imwrite("/home/crapito/coding/app-painting/src/outputImages/output.jpg", img))
        LOG(INFO) << "Saving img as output.jpg";
    else
        LOG(WARNING) << "Could not save file at specified destination";
    
}


// Set of utility functions to communicate with the main panel
void ToolPanel::SetImagePreview(wxImage &preview)
{
    cMain *cmain = (cMain*) m_parent->GetParent();
    cmain->SetImagePreview(preview);
}

bool ToolPanel::isImageLoaded() const
{
    cMain *cmain = (cMain*) m_parent->GetParent();
    bool res = cmain->isImageLoaded();
    return res;
}

wxString ToolPanel::GetImagePath() const
{
    cMain *cmain = (cMain*) m_parent->GetParent();
    wxString path = cmain->GetImagePath();
    return path;
}

std::tuple<int, int> ToolPanel::GetLowResDim() const
{
    cMain *cmain = (cMain*) m_parent->GetParent();
    std::tuple<int, int> dim = cmain->GetLowResDim();
    return dim;
}